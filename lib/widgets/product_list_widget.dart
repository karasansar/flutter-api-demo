import 'package:flutter/material.dart';
import 'package:flutter_api_app/models/product.dart';
import 'package:flutter_api_app/widgets/product_list_row_widget.dart';

class ProductListWidget extends StatefulWidget {
  // ignore: deprecated_member_use
  List<Product> products = new List<Product>();
  ProductListWidget(List<Product> product) {
    this.products = product;
  }

  @override
  State<StatefulWidget> createState() {
    return _ProducListState();
  }
}

class _ProducListState extends State<ProductListWidget> {
  @override
  Widget build(BuildContext context) {
    return buildProductList(context);
  }

  Widget buildProductList(BuildContext context) {
    // ignore: missing_return
    return Column(
      children: [
        SizedBox(height: 15.0,),
        SizedBox(
          height: MediaQuery.of(context).size.height / 1.3,
            child: GridView.count(
                crossAxisCount: 2,
                shrinkWrap: true,
                children: List.generate(widget.products.length, (index) {
                  return ProductListRowWidget(widget.products[index]);
                }))),
      ],
    );
  }
}
