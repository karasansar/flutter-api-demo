import 'package:flutter/material.dart';
import 'package:flutter_api_app/models/product.dart';

class ProductListRowWidget extends StatelessWidget{
  Product product;
  ProductListRowWidget(Product product) {
    this.product = product;
  }
  @override
  Widget build(BuildContext context) {
    return buildItemCard(context);
  }

  Widget buildItemCard(BuildContext context) {
    return InkWell(
      child: Card(
        child: Column(
          children: [
            Container(
              child: Image.network("https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/macbook-air-space-gray-select-201810?wid=904&hei=840&fmt=jpeg&qlt=80&op_usm=0.5,0.5&.v=1603332211000"),
              height: 130,
              width: MediaQuery.of(context).size.width/2,
            ),
            Text(product.productName),
            Text(product.unitPrice.toString() + " ₺")
          ],
        ),
      ),
      onTap: () {
        print(product.productName);
      },
    );
  }

}