import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_api_app/data/api/category_api.dart';
import 'package:flutter_api_app/data/api/product_api.dart';
import 'package:flutter_api_app/models/category.dart';
import 'package:flutter_api_app/models/product.dart';
import 'package:flutter_api_app/widgets/product_list_widget.dart';

class MainScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MainScreenState();
  }

}

class _MainScreenState extends State {
  // ignore: deprecated_member_use
  List<Category> categories = List<Category>();
  // ignore: deprecated_member_use
  List<Widget> categoryWidgets = List<Widget>();

  // ignore: deprecated_member_use
  List<Product> products = List<Product>();

  // ignore: deprecated_member_use
  List<Widget> productWidgets = List<Widget>();
  @override
  void initState() {
    getCategoryFromApi();
    getProducts();
    super.initState();
  }
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Alışveriş Sistemi", style: TextStyle(color: Colors.white, fontSize: 22.0),),
        backgroundColor: Colors.blueGrey,
        centerTitle: true,
      ),
      body: Padding(
        padding: EdgeInsets.all(15.0),
        child: Column(
          children: [
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(children: categoryWidgets,),
            ),
            ProductListWidget(products)
          ],
        ),
      ),
    );
  }

  void getCategoryFromApi() {
    CategoryApi.getCategory().then((res){
      setState(() {
        Iterable list = json.decode(res.body);
        this.categories = list.map((category) => Category.fromJson(category)).toList();
        getCategoryWidgets();
      });
    });
  }

  List<Widget> getCategoryWidgets() {
    for(int i = 0; i <= categories.length; i++){
      categoryWidgets.add(getCategoryWidget(categories[i]));
    }
    return categoryWidgets;
  }

  Widget getCategoryWidget(Category categori) {
    return FlatButton(
      child: Text(categori.categoryName, style: TextStyle(color: Colors.blue),),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
        side: BorderSide(color: Colors.blueGrey),
      ), onPressed: () {
        getProductsByCategory(categori);
    },
    );
  }

  void getProductsByCategory(Category categori) {
    ProductApi.getProductsCategoryId(categori.id).then((res) {
      setState(() {
        Iterable list = json.decode(res.body);
        this.products = list.map((data) => Product.fromjson(data)).toList();
      });
    });
  }


  void getProducts() {
    ProductApi.getProducts().then((res) {
      setState(() {
        Iterable list = json.decode(res.body);
        this.products = list.map((data) => Product.fromjson(data)).toList();
      });
    });
  }
}