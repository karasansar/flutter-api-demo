class Product {
  int id;
  int categoryId;
  String productName;
  int stock;
  double unitPrice;

  Product(
      this.id, this.categoryId, this.productName, this.stock, this.unitPrice);
  Product.fromjson(Map json) {
    id = json["id"];
    categoryId = json["categoryId"];
    productName = json["productName"];
    stock = json["stock"];
    unitPrice = double.tryParse(json["unitPrice"].toString());
  }

  Map toJson() {
    return {
      "id": id,
      "categroyId": categoryId,
      "productName": productName,
      "stock": stock,
      "unitPrice": unitPrice
    };
  }
}
