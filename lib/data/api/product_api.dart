import 'package:http/http.dart' as http;

class ProductApi {
  static Future getProducts() {
    return http.get("http://localhost:3000/product");
  }

  static Future getProductsCategoryId(int categoryId) {
    return http.get("http://localhost:3000/product?categoryId=${categoryId}");
  }
}
