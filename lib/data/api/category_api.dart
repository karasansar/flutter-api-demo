import 'package:http/http.dart' as http;

class CategoryApi {
  static Future getCategory() {
    return http.get("http://localhost:3000/category");
  }
}
